﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Wiyan.Code.PowerMSNRobotLibrary.Utility
{
    public class SystemInformation
    {
        public static string GetSystemInformation()
        {
            StringBuilder sb = new StringBuilder();
            string error = string.Empty;
            string result = null;

            // System version
            sb.AppendLine(string.Format("OSVersion:\t{0}", Environment.OSVersion));

            // Machine 
            sb.AppendLine(string.Format("Machine Name:\t{0}", Environment.MachineName));

            // User
            sb.AppendLine(string.Format("User:\t{0}", Environment.UserName));

            // IP Internal Address
            sb.AppendLine(string.Format("IP Internal:\t{0}", IPResolver.GetMyInternalIP().ToString()));

            // IP External Address
            IPAddress address = IPResolver.GetMyExternalIP(ref error);
            result = (address == null) ? error : address.ToString();
            sb.AppendLine(string.Format("IP External:\t{0}", result));
            
            // Remote assistance status
            string running = string.Empty;
            bool callResult = RAConfigurationHelp.GetConfiguration(ref running, ref error);
            result = (callResult == true) ? running : "Error: " + error;
            sb.AppendLine(string.Format("Remote Assistance:\t{0}", result));

            return sb.ToString();
        }
    }
}
