﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Wiyan.Code.PowerMSNRobotLibrary.Utility
{
    public class RAConfigurationHelp
    {
        private static string RAPATH_NT_5 = @"HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Terminal Server";
        private static string RAPATH_NT_6 = @"HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Remote Assistance";
        private static string ALLOW_GET_HELP_KEY = "fAllowToGetHelp";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="running"></param>
        /// <param name="errorInformation"></param>
        /// <returns></returns>
        public static bool GetConfiguration(ref string running, ref string errorInformation)
        {
            Version osVersion = Environment.OSVersion.Version;
            string raPath = null;

            if (osVersion >= new Version(6, 0))
            {
                // Windows vista/7/2008+
                raPath = RAPATH_NT_6;
            }
            else if (osVersion >= new Version(5, 0))
            {
                raPath = RAPATH_NT_5;
            }
            else
            {
                errorInformation = "Not supported OS detected";
                return false;
            }

            // Get setting from registry
            try
            {
                object oResult = Registry.GetValue(raPath, ALLOW_GET_HELP_KEY, null).ToString();
                int enable = int.Parse(oResult.ToString());
                running = enable == 1 ? "Enabled" : "Disabled";
            }
            catch (Exception e)
            {
                errorInformation = e.Message;
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="running"></param>
        /// <param name="errorInformation"></param>
        /// <returns></returns>
        public static bool SetConfiguration(bool enable, ref string errorInformation)
        {
            Version osVersion = Environment.OSVersion.Version;
            string raPath = null;
            errorInformation = string.Empty;

            if (osVersion >= new Version(6, 0))
            {
                // Windows vista/7/2008+
                raPath = RAPATH_NT_6;
            }
            else if (osVersion >= new Version(5, 0))
            {
                raPath = RAPATH_NT_5;
            }
            else
            {
                errorInformation = "Not supported OS detected";
                return false;
            }

            // Get setting from registry
            try
            {
                Registry.SetValue(raPath, ALLOW_GET_HELP_KEY, enable == true ? 1 : 0);
            }
            catch (Exception e)
            {
                errorInformation = e.Message;
                return false;
            }

            return true;
        }
    }
}
