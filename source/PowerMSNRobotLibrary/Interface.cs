﻿using System;
using System.Collections.Generic;
using System.Text;

using Wiyan.Code.PowerMSNRobotLibrary.Commands;
using Wiyan.Code.PowerMSNRobotLibrary.Manager;
using System.Collections;

namespace Wiyan.Code.PowerMSNRobotLibrary.SDK
{
    public struct CommandContext
    {
        public string Name;
        public string Description;
        public EventHandler OnCommand;

        public CommandContext(string name, string description)
        {
            this.Name = name;
            this.Description = description;
            this.OnCommand = null;
        }

        public CommandContext(string name, string description, EventHandler commandDelegate)
        {
            this.Name = name;
            this.Description = description;
            this.OnCommand = commandDelegate;
        }
    }

    public interface ICommandResultBase
    {
        bool Status { get; set; }
        string ReplyMessage { get; set; }
    }

    public interface ICommandAsync
    {
        bool RunASync(ref Guid token, object[] parameters);
        ICommandResultBase WaitAsyncResult(Guid token);
    }

    public interface ICommandProvider
    {
        //ICommandProvider[] SupporttedCommands { get; }
        string Name { get; }
        string Description { get; }

        // Session interface
        bool Initialize(object parameter);

        // Command interface
        ICommandResultBase Run(object[] parameters);
    }
}
