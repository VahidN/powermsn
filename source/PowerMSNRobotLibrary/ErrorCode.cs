﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;

using PowerMSNRobotLibrary;

namespace Wiyan.Code.PowerMSNRobotLibrary
{
    public enum ErrorCode
    {
        Success = 0,
        Error = 1,
        WorkerThreadAlreadyStarted = 2,
        WorkerThreadNotRunning = 3,
        RedirectionIOFailed = 4,
        InvalidReplyMessageDelay = 5,
        InvalidWorkerThreadParameter = 6,
        MSRAAUTONOTSUPPORTEDAUTOCONFIG = 7,
        MsraWindowsNotFound = 8,
        MsraAllowWindowsNotFound = 9,
        MsraAllowWindowsOKButtonNotFound = 10,
        MsraOKButtonNotSupportInvokePattern = 11,
        MsraProcessKilled = 12
    }

    public class ErrorInformation
    {
        // powermsn_error_[ErrorCode]
        private static string _format = "powermsn_error_{0}";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetDetailError(ErrorCode code)
        {
            string resourceKey = string.Format(_format, code.ToString());
            string errorInResource = Resource.ResourceManager.GetString(resourceKey);

            return errorInResource == null ? code.ToString() : errorInResource;
        }
    }
}
