﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wiyan.Code.PowerMSNRobotLibrary.Utility;
using Wiyan.Code.PowerMSNRobotLibrary.Commands;
using Wiyan.Code.PowerMSNRobotLibrary.SDK;
using System.Diagnostics;
using Wiyan.Code.PowerMSNRobotLibrary;

namespace robot_tdd
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Get
            //string s = SystemInformation.GetSystemInformation();
            //Console.WriteLine(s);

            //string error = string.Empty;
            //RAConfigurationHelp.SetConfiguration(false, ref error);
            //s = SystemInformation.GetSystemInformation();
            //Console.WriteLine(s);

            //error = string.Empty;
            //RAConfigurationHelp.SetConfiguration(true, ref error);
            //s = SystemInformation.GetSystemInformation();
            //Console.WriteLine(s);

            //Console.ReadLine();

            //ICommandProvider command = CommandLineCommand.GetInstance();

            //IExclusiveCommandProvider iCommandProvider = command as IExclusiveCommandProvider;
            //iCommandProvider.Start(null);
            //while (true)
            //{
            //    string input = Console.ReadLine();

            //    if (input == "exit")
            //        break;

            //    iCommandProvider.ProcessSubCommand(input);
            //}

            //iCommandProvider.Close();

            VistaRemoteAssistanceHelper rah = new VistaRemoteAssistanceHelper(null);
            string ticketPath = @"D:\share\ra\tdd";
            rah.GenerateRATicket("User@123", ref ticketPath);

            while (true)
            {
                string cmd = Console.ReadLine();
                ErrorCode error = ErrorCode.Success;

                if (cmd == "allow")
                    error = rah.Allow();
                else if (cmd == "stop")
                    error = rah.StopRASession();

                Console.WriteLine("Command result: {0}", error.ToString());
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }
    }
}
